﻿using Npgsql;
using SimpleMigrations;
using SimpleMigrations.Console;
using SimpleMigrations.DatabaseProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TripPlannerMigrations
{
    class Program
    {
        static void Main(string[] args)
        {
            var connectionString = @"User ID=trip-planner-service;Password=trip-planner-password;Host=localhost;Port=5432;Database=trip-planner-dev;";
            var migrationsAssembly = typeof(Program).Assembly;
            using (var connection = new NpgsqlConnection(connectionString))
            {
                var databaseProvider = new PostgresqlDatabaseProvider(connection);
                var migrator = new SimpleMigrator(migrationsAssembly, databaseProvider);

                var consoleRunner = new ConsoleRunner(migrator);
                consoleRunner.Run(args);
                Console.ReadLine();
            }
        }
    }
}
