﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nancy;
using Nancy.Testing;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TripPlannerWeb.Models;
using TripPlannerWeb.Modules;
using TripPlannerWeb.Services;

namespace TripPlannerTests
{
    [TestClass]
    public class UserModuleTests
    {
        [TestMethod]
        public void PostValidUserForm()
        {
            var userForm = new UserForm()
            {
                Username = "username",
                Password = "password"
            };

            var user = new User()
            {
                Username = "username",
                Id = 1
            };

            var userService = Substitute.For<IUserService>();
            userService.CreateUser(Arg.Any<UserForm>()).Returns(user);

            var bootstrapper = new ConfigurableBootstrapper((with) => with.Dependency(userService).Module<UsersModule>());
            var browser = new Browser(bootstrapper);

            var response = browser.Post("/users/create", (with) => {
                with.HttpRequest();
                with.FormValue("Username", "username");
                with.FormValue("Password", "password");
            });

            userService.Received().CreateUser(Arg.Any<UserForm>());
            response.ShouldHaveRedirectedTo($"/users/@{user.Id}");
        }
    }
}
