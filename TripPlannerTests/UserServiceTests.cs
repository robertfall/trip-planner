﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TripPlannerWeb.Services;
using TripPlannerWeb.Models;
using Npgsql;

namespace TripPlannerTests
{
    [TestClass]
    public class UserServiceTests
    {
        void DbTest(Action<NpgsqlConnection> test)
        {
            var connectionString = @"User ID=trip-planner-service;Password=trip-planner-password;Host=localhost;Port=5432;Database=trip-planner-dev;";
            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction())
                {
                    test(connection);
                    transaction.Rollback();
                }
            }
        }

        [TestMethod]
        public void CreatesUser()
        {
            DbTest((connection) =>
            {
                var service = new UserService(connection);
                var user = service.CreateUser(new UserForm()
                {
                    Username = "testuser",
                    Password = "testpassword"
                });

                Assert.AreNotEqual(user.Id, 0);
                Assert.IsNotNull(user.Salt);
                Assert.IsNotNull(user.HashedPassword);

                var retrievedUser = service.FindByUsername("testuser");
                Assert.AreEqual(user.Id, retrievedUser.Id);
                Assert.AreEqual(user.Username, retrievedUser.Username);
                Assert.AreEqual(user.HashedPassword, retrievedUser.HashedPassword);
                Assert.AreEqual(user.Salt, retrievedUser.Salt);
            });
        }

        [TestMethod]
        public void RetrievesUser()
        {
            DbTest((connection) =>
            {
                var service = new UserService(connection);
                var user = service.CreateUser(new UserForm()
                {
                    Username = "testuser",
                    Password = "testpassword"
                });

                var retrievedUser = service.FindByUsername("testuser");
                Assert.AreEqual(user.Id, retrievedUser.Id);
                Assert.AreEqual(user.Username, retrievedUser.Username);
                Assert.AreEqual(user.HashedPassword, retrievedUser.HashedPassword);
                Assert.AreEqual(user.Salt, retrievedUser.Salt);
            });
        }
    }
}