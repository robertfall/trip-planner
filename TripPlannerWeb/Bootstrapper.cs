﻿using Nancy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nancy.TinyIoc;
using Npgsql;
using Nancy.Bootstrapper;

namespace TripPlannerWeb
{
    public class Bootstrapper : DefaultNancyBootstrapper
    {
        const string connectionString = @"User ID=trip-planner-service;Password=trip-planner-password;Host=localhost;Port=5432;Database=trip-planner-dev;";

        protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
        {
            Nancy.Security.Csrf.Enable(pipelines);
            base.ApplicationStartup(container, pipelines);
        }

        protected override void ConfigureApplicationContainer(TinyIoCContainer container)
        {
            container.Register<System.Data.IDbConnection>((a, b) => new NpgsqlConnection(connectionString));
            base.ConfigureApplicationContainer(container);
        }
    }
}
