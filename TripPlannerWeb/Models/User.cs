﻿namespace TripPlannerWeb.Models
{
    public class User
    {
        public uint Id { get; set; }
        public string Username { get; set; }
        public string HashedPassword { get; set; }
        public string Salt { get; set; }
    }
}
