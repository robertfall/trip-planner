﻿using Nancy.Validation;
using System.Dynamic;
using TripPlannerWeb.Extensions;

namespace TripPlannerWeb.Models
{
    public class UserForm
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public ModelValidationResult Validation { get; set; }
        public bool IsValid { get { return Validation == null || Validation.IsValid; } }
        public ExpandoObject FormattedErrors { get { return Validation.FormattedErrors.ToExpando(); } }
    }
}
