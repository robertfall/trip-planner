﻿using FluentValidation;

namespace TripPlannerWeb.Models.Validations
{
    public class UserFormValidator : AbstractValidator<UserForm>
    {
        public UserFormValidator()
        {
            RuleFor(request => request.Username).NotEmpty().WithMessage("You must specify a username.");
            RuleFor(request => request.Password).NotEmpty().WithMessage("You must specify a password.");
            RuleFor(request => request.Password).Must((p) => p == null || p.Length > 7).WithMessage("The password must be at least 8 characters long.");
        }
    }
}
