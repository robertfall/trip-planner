﻿using Nancy;
using Nancy.ModelBinding;
using Nancy.Validation;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TripPlannerWeb.Models;
using TripPlannerWeb.Services;

namespace TripPlannerWeb.Modules
{
    public class UsersModule : NancyModule
    {
        public UsersModule(IUserService userService)
        {
            Get["/users/new"] = _ => View["new", new UserForm()];
            Post["/users/create"] = _ =>
            {
                var form = this.Bind<UserForm>();
                form.Validation = this.Validate(form);
                if (!form.IsValid)
                {
                    return Negotiate
                      .WithView("new")
                      .WithModel(form)
                      .WithStatusCode(HttpStatusCode.BadRequest);
                }

                var user = userService.CreateUser(form);
                return Response.AsRedirect($"/users/@{user.Id}");
            };
        }
    }
}
