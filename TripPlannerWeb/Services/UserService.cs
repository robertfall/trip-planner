﻿using Dapper;
using TripPlannerWeb.Models;
using System;
using System.Data;
using System.Linq;
using System.Security.Cryptography;

namespace TripPlannerWeb.Services
{
    public interface IUserService
    {
        User CreateUser(UserForm user);
        User FindByUsername(string username);
    }

    public class UserService : IUserService
    {
        private IDbConnection connection;

        public UserService(IDbConnection connection)
        {
            this.connection = connection;
        }
        public User CreateUser(UserForm form)
        {
            var newUser = UserFromUserForm(form);
            var id = connection.Query<uint>(
                    @"
                        INSERT INTO users(username, hashed_password, salt)
                        VALUES (@Username, @HashedPassword, @Salt);
                        SELECT currval(pg_get_serial_sequence('users','id'))
                      ", newUser).First();

            newUser.Id = id;
            return newUser;
        }

        public User FindByUsername(string username)
        {
            return connection.Query<User>(
                @"
                    SELECT id, username, hashed_password as HashedPassword, salt 
                    FROM users 
                    WHERE username = @Username
                ", new { Username = username }).First();
        }

        private User UserFromUserForm(UserForm form)
        {
            var salt = generateSalt();
            var seed = "SECRET_SEED";

            var saltedPassword = String.Concat(seed, salt, form.Password);
            var hashedPassword = CryptoHelper.Crypto.HashPassword(saltedPassword);
            return new User()
            {
                Username = form.Username,
                Salt = salt,
                HashedPassword = hashedPassword,
            };
        }

        private string generateSalt()
        {
            var salt = new byte[32];
            using (var random = new RNGCryptoServiceProvider())
            {
                random.GetNonZeroBytes(salt);
            }

            return Convert.ToBase64String(salt);
        }
    }
}
